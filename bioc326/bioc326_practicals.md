---
Author: Pierre Khoueiry - 'PhD (Computational Genomics Lab)'  
Title: BIOC 326 Practicals
---

**Dear BIOC 326 students. Welcome to our practical session series. I
hope that you will enjoy it.**


***It is stricly forbidden to share or distribute the content of this tutorial***

# Table of Contents
<details>
<summary>UCSC genome browser tutorial</summary>

UCSC genome browser tutorial
========================================

    Learning objectives:
    1.  Learn how to use a genome browser and obtain transcript/gene information
    2.  Learn how to check for a variation affecting a certain gene.
    3.  Learn how to get the DNA sequence of a region of interest
    4.  Check for known interactions with my gene
    5.  Run a blat/blast search and pairwise sequence alignment 
    6.  Download data in batch

**Task 1: Check for transcript and DNA sequences**

-   Go to : <https://genome.ucsc.edu/>
-   Go to Genomes and select hg38
-   Search for the hba1 gene (The human alpha globin gene)
	*Check on the net for the role of hba1 and its associated diseases*
-   What are the different tracks that you see?
-   Disable/enable some of them and check the effect
-   Check for the effect of choosing between "Hide", "Pack" and other
    available options for each track
-   Zoom out to investigate the surrounding locus and notice the
    direction of the gene (positive or negative strand)
-   Click on one of the transcripts and see its full description
-   Go back Check the conservation track: What do you see and why ? (you
    can load it from the Comparative Genomics section below)
-   In the menu, chose ”view” -&gt; DNA -&gt; click on ”get DNA” to obtain the DNA
    sequence of the genomic locus

**Task 2: Check for mutations affecting my gene of interest**

-   Go back to the genome browser
-   In the Phenotype and Literature section, select track ”ClinVar
    Variants”
-   Choose the “pack” option for display
-   Go back to the genome browser and zoom in to the first exon to see
    the variations
-   Click on the variants in bold C.43T&gt;Y or T&gt;C (Highlighted in red) and see its full
    description.
-   What nucleotide does this mutation affect?
-   What is its clinical significance?

**Task 3: Get all known interactions with my gene**

-   In the ”Tools” tab, select “Gene Interactions”
-   Search for hba1 (or your favorite gene) to find its interacting
    partners
-   Hover over each symbol to get more information
-   Hover over the edges to get more information regarding the
    interaction evidences
-   What is the major difference between the blue and the grey edges?

------------------------------------------------------------------------

**Task 4: Download data in batch**

-   Go to : <https://genome.ucsc.edu/>
-   Select Tools -&gt; Table browser
-   For “track”, select NCBI RefSeq genes
-   Click on “describe table schema” and explain what you see
-   Go back and click on “summary/statistics”
-   Go back and chose “BED” as output format
-   Click on ”get output”
-   Select ”Upstream by” and then put “1000”
-   Click “get BED” and Describe what you observe

------------------------------------------------------------------------

**Task 5: Showing custom information on the genome browser**

    - This way allows you to load any custom or personal information in order to see it in its genomics context on a genome browser
    - You will get all the information that exists in the database by choosing them from the “tracks” below

-   Copy (Ctr-C/Cmd-C) the first 20 lines or so from the BED output
-   Open a new UCSC genome browser page
-   under the genome browser, click on "add custom tracks"
-   Now, click on "add custom tracks"
-   Paste the copied data into “Paste URLs or data”
-   Click on “submit”
-   Click on "go"
-   Zoom out 10x: What do you observe?

------------------------------------------------------------------------

**Task 6: Performing BLAST search** 

"BLAST finds regions of similarity
between biological sequences. The program compares nucleotide or protein
sequences to sequence databases and calculates the statistical
significance."

-   Visit the following link and follow the tutorial:
    <https://digitalworldbiology.com/blast>

-   Visit <https://www.ncbi.nlm.nih.gov/BLAST/> and perform a blast
    using the **hba1** gene sequence.

NB: The sequence of the gene can be obtained using UCSC genome browser
(previous tutorial)

Try to get the sequence yourself first. If you face problems, use the
following one:

```{.example}
>hg38_dna range=chr16:176680-177522 5'pad=0 3'pad=0 strand=+ repeatMasking=none
ACTCTTCTGGTCCCCACAGACTCAGAGAGAACCCACCATGGTGCTGTCTC
CTGCCGACAAGACCAACGTCAAGGCCGCCTGGGGTAAGGTCGGCGCGCAC
GCTGGCGAGTATGGTGCGGAGGCCCTGGAGAGGTGAGGCTCCCTCCCCTG
CTCCGACCCGGGCTCCTCGCCCGCCCGGACCCACAGGCCACCCTCAACCG
TCCTGGCCCCGGACCCAAACCCCACCCCTCACTCTGCTTCTCCCCGCAGG
ATGTTCCTGTCCTTCCCCACCACCAAGACCTACTTCCCGCACTTCGACCT
GAGCCACGGCTCTGCCCAGGTTAAGGGCCACGGCAAGAAGGTGGCCGACG
CGCTGACCAACGCCGTGGCGCACGTGGACGACATGCCCAACGCGCTGTCC
GCCCTGAGCGACCTGCACGCGCACAAGCTTCGGGTGGACCCGGTCAACTT
CAAGGTGAGCGGCGGGCCGGGAGCGATCTGGGTCGAGGGGCGAGATGGCG
CCTTCCTCGCAGGGCAGAGGATCACGCGGGTTGCGGGAGGTGTAGCGCAG
GCGGCGGCTGCGGGCCTGGGCCCTCGGCCCCACTGACCCTCTTCTCTGCA
CAGCTCCTAAGCCACTGCCTGCTGGTGACCCTGGCCGCCCACCTCCCCGC
CGAGTTCACCCCTGCGGTGCACGCCTCCCTGGACAAGTTCCTGGCTTCTG
TGAGCACCGTGCTGACCTCCAAATACCGTTAAGCTGGAGCCTCGGTGGCC
ATGCTTCTTGCCCCTTGGGCCTCCCCCCAGCCCCTCCTCCCCTTCCTGCA
CCCGTACCCCCGTGGTCTTTGAATAAAGTCTGAGTGGGCGGCA
```

------------------------------------------------------------------------

**Task 7: Pairwise sequence alignment** 

"Pairwise sequence alignment is
used to identify regions of similarity that may indicate functional,
structural and/or evolutionary relationships between two biological
sequences (protein or nucleic acid)."

-   Get the **protein** sequence of the human Hba1 gene and it
    corresponding mouse orthologue Hba-a1. (Use UCSC genome browser)

**HBA1**

``` {.example}
>ENST00000320868.9 (HBA1) length=142
MVLSPADKTNVKAAWGKVGAHAGEYGAEALERMFLSFPTTKTYFPHFDLSHGSAQVKGHG
KKVADALTNAVAHVDDMPNALSALSDLHAHKLRVDPVNFKLLSHCLLVTLAAHLPAEFTP
AVHASLDKFLASVSTVLTSKYR
```

**HBA-a1**

``` {.example}
>uc007ijk.2 (Hba-a1) length=142
MVLSGEDKSNIKAAWGKIGGHGAEYGAEALERMFASFPTTKTYFPHFDVSHGSAQVKGHG
KKVADALANAAGHLDDLPGALSALSDLHAHKLRVDPVNFKLLSHCLLVTLASHHPADFTP
AVHASLDKFLASVSTVLTSKYR
```

-   Visit the follow <https://www.ebi.ac.uk/Tools/psa/emboss_needle/>
-   Take your time to check the different options
-   Paste in the both protein sequences in their corresponding fields and
    submit
-   Check the output and explain

</details>

---

<details>
<summary>Running Quality check</summary>

Running Quality check
=================================

Objectives: In this tutorial, we will explore different formats used in
NGS experiments. For this, we will used our Galaxy sever to visualize
files and use some installed tools for format conversion. We will focus
on major formats including Fastq, BAM/SAM, BED and bigwig.

-   Go to <https://usegalaxy.org.au/>

**Section I: Fastq file visualization and trimming**

-   Go to "Shared Data" -&gt; "Histories" -&gt; then look for __bioc326__ , and click on it
-   From the top right corner, click on "+" sign to import the history
-   Click on the file name "BRD416\_chrX.fastq" to obtain more
    information about it (metadata)
-   Visualize the content of the files and comment
-   Run FastQC analysis on both files ("BRD416\_chrX.fastq" and "sample\_1.fastq") 
    and comment and check the different results obtained 
    including "sequence length", base quality
    ...
-   Visualize and compare the results of the following two runs

**As you notice, sample\_1.fastq has some sequencing issue and needs to
be pre-processed prior to alignment**

-   Which decision should be taken and how to proceed?
-   Go back to galaxy and search for the "Trim" tool under "Text Manipulation" section in the Tools panel
    (left panel) and load it
-   Choose the **sample\_1.fastq** dataset and trim it by removing **30**
    bases from the **END** of the reads (**read the documentation well
    to chose the right options**) NB: Be aware of choosing FASTQ as
    format NB: Be aware of keeping 1 in **Trim from the beginning up to
    this position**
-   Rerun FastQC on the newly generated trimmed Fastq file (the output
    of the "Trim" program)
-   Compare the results with the FastQC report of the original file and
    comment

------------------------------------------------------------------------

</details>

---



<details>
<summary>Interrogating and visualizing ENCODE data</summary>

Interrogating and visualizing ENCODE data
=====================================================

    Objectives:
    In this tutorial, we will explore the ENCODE project database to search and 
    visualize genomics data generated by different labs, as part of the ENCODE project.
    In our tutorial, we will learn how to search, find and download data for a specific assay (i.e PolII ChIP-seq on K562 cells).
    For this, we will focus on one precise transcription factor called CTCF. 
    CTCF is a major transcriptional repressor in mammalian cells. 
    It is implicated in several cellular mechanisms including transcription regulation, 
    insulation and has been showing lately to regulate chromatin architecture by
    stabilizing loop formation and 3D structures.

-   Login to <https://www.encodeproject.org/>
-   Explore the different applications assayed as part of the ENCODE
    project. As you notice, all genomic features are assayed by the
    ENCODE project. By clicking on each square/assay type, you check the
    number of projects and biosamples available.
-   Explore RNA-seq assays
-   Click on Filtered data Matrix and explore the output

The webpage will show the experiment matrix where we can filter for
different criteria including organism, Cell/tissue type, Organ, file
format …

-   We are interested in ChIP-seq assays. For this, clear all the filters and chose ChIP-seq
-   explore the output in Filtered data Matrix.
-   Go to "Filtered Data Matrix" and choose to show all TF ChIP-seq
    performed on K562 (as part of the ENCODE project)
-   Check the left panel showing the different filters applied and
    allowing us to add/remove filters

We are interested in insulator elements identified in K562 cells. For
this, we will check for the availability of ChIP-seq on CTCF
transcription factor (a known insulator element)

-   Search for CTCF in "target of assay"
-   Check the **Available file types** box in the left panel to know about the different
    datasets available for CTCF in K562.

We notice here that 5 results are available (as of Jan 2022). They
correspond to different CTCF ChIP-seq experiments performed on K562
cells. The information on the experiment, the lab that performed it and
the project can be seen.

-   Choose the data from the lab of "Michael Snyder, Stanford".
-   Check the experiment summary and describe what you see.
    Specifically, check the Biological or Isogenic replicates section.
-   Zoom in and out in the **Association graph** panel to check how
    those files were generated and the pipeline used
-   In the **File details** panel, select the "ENCFF694KDC" in
    Accession. There, you can see all the information linked to this
    file, the way it was generated (pipeline) and many other associated
    metadata
-   Eventually, you can download the file here. We will skip this step

We will visualize now the data on the UCSC genome browser

-   Go back to data sets from the Snyder lab and select, from the same
    page, the Dataset link "ENCSR000EGM". follow this link if you got
    lost <https://www.encodeproject.org/experiments/ENCSR000EGM/>
-   This will bring us to the page where we accessed the BED file
-   Under **File details**, choose "GRCh38" and click on **Visualize** after
    choosing **UCSC** as a browser in the dropdown menu.
-   Check how the data looks like by working on the browser, as we
    previously saw (zoom in/out, add/remove tracks, ...)
-   Add/remove tracks from the browser page to make clearer or to add
    information that you need.

Adding additional tracks from the ENCODE project for visualization and
comparison.

-   Go back to ENCODE project website
-   Search for POLR2A (polymerase II subunit A) ChIP-seq on K562 (i.e
    Experiment ENCSR000EHL from the same lab)
-   Similarly, as before, visualize the data on UCSC genome browser and
    compare binding of CTCF and PolII.
-   Search for the gene *kmt2e* and check CTCF and PolII signal in the
    surrounding and specifically on the TSS.

------------------------------------------------------------------------

</details>

---

<details>
<summary>ChIP-seq peak calling</summary>

ChIP-seq peak calling
=================================

    Aim: This tutorial aims at teaching students principles of peak calling using MACS2 and Galaxy.
    The dataset is from Khoueiry et al., 2019 from a ChIP-seq on K562 cell lines. We will use pre-aligned files (bam files) since alignment can be slow.

**Importing and visualizing alignment files content**

-   Go to <https://usegalaxy.org.au/>
-   Go to "Shared Data" -&gt; "Histories" -&gt; then look for __bioc326__ , and click on it
-   From the top right corner, click on "+" sign to import the history
-   Make sure to get the information about the file content by including 
    the header information
-   Comment the output and describe it.

**Peak calling using MACS2**

-   Select "MACS2 callpeak" from the Tools list in the left panel
    (Search for it for an easier access
-   Read carefully the documentation specifically related to the
    functionality of the program and its usage.
-   Under "ChIP-Seq Treatment File" choose
    "BRD416_chrX.bam" as a ChIP file
-   Under "ChIP-Seq Control File" choose
    "Input24_chrX.bam" as a control file
-   Choose the right genome in "Effective genome size" (H. sapiens).
-   In Outputs select only "Peaks as tabular file"
-   Check the peaks (narrowPeaks), in BED format, when the run is finished, by
    clicking on the "eye" for the corresponding analysis (right panel)

NB: Search the Net for a description of the BED file format

**Question: How many peaks did the program find?**

-   Describe the content of the output file.
-   Always in the right panel, click on the "Pen" sign for the BED
    format output. This will allow us to see the attributes and change
    the format of the file for an easier inspection
-   Click on "Convert Format" and choose "bedstrict (using 'interval-to-bedstrict')"
-   Read the parameters carefully
-   Click on Convert to execute
-   Download the file on your Desktop (you have to figure out how to do
    so)
-   Go to UCSC genome browser under <https://genome.ucsc.edu/index.html>
-   Under Genome choose the right species and genome build for the data
    you want to visualize.
-   under the genome browser, click on "add custom tracks" -> "add custom tracks" and upload the file that you just downloaded (under browse) after you click on "submit"
-   click on the "go" button
-   Zoom out to see some of the peaks. Be sure to be on ChrX since the
    data is for Chromosome X only
-   Change the track color (peaks color) from black to any color of your
    choice, by searching the documentation or the net on how to do so
</details>

---


<details>
<summary>Motif search and discovery: Identifying DNA-binding motifs for the protein of interest</summary>

Motif search and discovery: Identifying DNA-binding motifs for the protein of interest
==================================================================================================

    Aim: In this tutorial, we will use publicly available datasets to discover and search for transcription factor binding motif (i.e overrepresented motifs).
    The tools we will be using are name MEME and FIMO.

    Two procedures will be followed:
    1) Motif discovery
    2) Motif search

Needed material can be downloaded from BIOC 326 moodle, Module B, Lecture 1-7 (MEME suite materials)

**1) Motif discovery**

    *Needed material:*
    Sequence file "sequences_for_motif_discovery.txt"

-   Open the file "sequences\_for\_motif\_discovery.txt" and check the
    sequences in it.
-   Describe the format of the file content.
-   Go to MEME suite on the link <http://meme-suite.org/>
-   In Motif Discovery, select "MEME"
-   Upload the sequences file "sequences\_for\_motif\_discovery.txt"
-   Spend some time exploring the different parameters
-   Under "Select the number of motifs", choose "3"
-   Open "Advance options"
-   Define 15 as "Maximum width"
-   Click on "Start Search"
-   When run is done, go to "MEME HTML output"
-   Explore the results obtained

**2) Motif Search using FIMO**

    Needed material:
    Sequences file "mm9_tss_500bp_sampled_1000.fa"
    Motif file "MA0108.1_TBP.txt"

-   Go to <http://meme-suite.org/tools/fimo>
-   Upload the motif file "MA0108.1\_TBP.txt" in section "Input the
    motifs"
-   Choose "upload sequences" from the dropdown menu in "Input the
    sequences"
-   Upload the sequences file "mm9\_tss\_500bp\_sampled\_1000.fa" under
    "Input the sequences" section
-   Click on "Start Search" to run motif search
-   Explore the results under "FIMO HTML output"
-   Describe the results obtained
-   Read carefully the description given for the p-value and the q-value

**JASPAR - The high-quality transcription factor binding profile
database**

    Jaspar is a widely used database for obtained matrices of Transcription factor binding profiles. 

-   Go to <http://jaspar.genereg.net/>
-   Type Tbx5 in "Browse/search a JASPAR database"
-   Explore the results

------------------------------------------------------------------------

</details>

---

<details>
<summary>Evaluating replicates using deeptools</summary>

Evaluating replicates using deeptools
=================================================

    In this tutorial we will do the following:
    1.  Learn how to import data required for analysis in Galaxy.
    2.  Create a correlation plot and a principal component analysis plot (PCA plot).
    3.  Create a heatmap for scores associated with genomic regions.
    4.  Create a profile plot for score distributions across genomic regions.

1: Import history

- From https://usegalaxy.org.au/, go to "Shared data" -> "Histories" and search for BIOC326
- On the top right corner, click the "+" button to import the history containing the files for our tutorial

2: Create a correlation plot and a PCA plot

**Comparing different bam files by creating multi bam summary**

-   Choose "Deeptools" as a tool and take 5 min to explore the different
    utilities that it offers. Also check
    <https://deeptools.readthedocs.io/en/latest/index.html>
-   Select "MultiBamSummary" and use it to compare all loaded bam files
-   Specify "chrX" as "Region of the genome to limit the operation NB:
    "chrX" is case sensitive.
-   Run the tool

**Drawing the correlation between the different bam files**
-   Go to **Deeptools** -&gt; **plotCorrelation**
-   Choose the newly generated matrix
-   Choose **Spearman** as correlation method
-   Execute the script
-   Analyze the data
-   Re-run the script by choosing **Pearson** as correlation method and
    using "Pearson" as title
-   Comment the results obtained
-   Re-run the script by specifying **Scatter** as output method instead
    for Heatmap

**Creating a PCA plot**
-   Explore the help below
-   Go to **Deeptools** -&gt; **plotPCA**
-   Choose the **multi bam file**
-   Run the script by using **Execute**
-   comment the content of the plot

3: Create a heatmap for scores associated with genomic regions.

**Creating the "BigWig" file first**

-   Explore the help below
-   Go to "Deeptools" -&gt; "bamCoverage".
-   Choose a BAM file from BRD416 or BRD421
-   Choose **hg19** genome.
-   Specify **chrX** as genomic region and "Execute".

**Creating the matrix from "Bigwig" file**
-   Explore the help below
-   Go to "Deeptools" -&gt; "computeMatrix".
-   Choose the BED file and the Bigwig file (output of bamCoverage) or the preloaded bigwig file in the history.
-   Under "computeMatrix, we have two main output options. choose
    "reference-point" and then "Execute".

**Plotting the heatmap and the profile plots**
-   Go to "Deeptools" -&gt; "plotHeatmap".
-   Choose the generated matrix and "Execute".

-   Go to "Deeptools" -&gt; "plotProfile".
-   Choose the generated matrix and "Execute".
------------------------------------------------------------------------
</details>

---


