
Day 1. Basic R Manipulation
===========================

by Khoueiry P (pk17@aub.edu.lb), `r Sys.time()`.  
Some taks are inspired from "https://github.com/eilslabs/teaching/"

# Basic data format

There are following basic data types in R: vectors, matrices, data frames and lists.
See https://www.statmethods.net/input/datatypes.html for explanation.

## {.tabset}

### task
Constuct a numeric vector from 1 to 10.

### solution

```{r}
x = 1:10
x
```

## {.tabset}

### task

Constuct a numeric vector of 3, 6, 2, 9, 1.

### solution

```{r}
x = c(3, 6, 2, 9, 1)
x
```

## {.tabset}

### task

Constuct a character vector of year months

### solution

```{r}
x = c("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")
x
```

## {.tabset}

### task

Constuct a numeric vector of the number of days in each month of the year then extract the months with 31 days in them

### solution task 1

```{r}
x = c("January" = 31, "February" = 28, "March" = 31, "April" = 30, "May" = 31, "June" = 30, "July" = 31, "August" = 31, "September" = 30, "October" = 31, "November" = 30, "December" = 31)
x
```

### solution task 2
```{r}
which(x == 31)
```

## {.tabset}

### task

Access the *3rd* month of the year and then the *3rd and  7th* months of the year

### solution task 1

```{r}
x[3]
#or 
x["March"]
```

### solution task 2
```{r}
x[c(3,7)] 
#or
x[c("March", "July")]
```


## {.tabset}

### task
Use a set of availabe funtions in R to get the below information for a **vector of elements containing 1000 random numerical values**  
* Constructing the vector and Checking the first 10 elements of the vector  
* Vector length  
* Maximum and minimum values (for a numeric vector)  
* Sum of values in a vector  
* Multiplying all values in a vector by 20  
* Log10 of values in a numeric vector  
* Get the summary of all x values
* Draw x values and comment. Also add a vertical line at "0"

### solution task 1
```{r}
x = rnorm(1000)
head(x, 20)
#or
x[1:20]
```

### solution task 2
```{r}
length(x)
```

### solution task 3
```{r}
max(x)
min(x)
```

### solution task 4
```{r}
sum(x)
```

### solution task 5
```{r}
x*10
```

### solution task 6
```{r}
log10(x)
```

### solution task 7
```{r}
summary(x)
```


## {.tabset}

### task

Construct a matrix of random values with 10 rows and 5 columns.

Consider `rnorm()`, `sample()`, `runif()` for random value generation. If you don't knonw
how to use these functions, type `?rnorm`, `?sample` and `?runif` for documentations.

### solution

```{r}
m = matrix(rnorm(50), nrow = 10)
m = matrix(runif(50), nrow = 10)
m = matrix(sample(10, 10, replace = TRUE), nrow = 10)
```



## {.tabset}

### task
Draw a set of random numbers following a normal distribution

### solution

```{r}
x = rnorm(1000)
# plotting
plot(density(x))
# Add a vertical line at 0
abline(v = 0)
# plotting by using the red color 
plot(density(x), col="red")
# Explore the different parameter that the plot function accepts
print("?plot")
```


Day 2. Basic R Manipulation
===========================


## {.tabset}

### task

Construct a matrix of random values with 5 rows and 5 columns.
We will consider the `sample()` function for that purpose
NB: we could have used `rnorm()` or other functions.

First we will read the help for function `sample()` 

### solution

```{r}
m = matrix(sample(100, 25, replace = TRUE), nrow = 5)
```

<!-- ## {.tabset} -->

<!-- ### task -->
<!-- Construct a dataframe of Lebanese major cities with their corresponding Governate and population size along with their ranking (source: https://en.wikipedia.org/wiki/Lebanon) -->

<!-- ### solution -->
<!-- ```{r} -->
<!-- 	cities = data.frame( -->
<!-- 		rank = c(1:10), -->
<!-- 		city = c("Beirut", "Tripoli", "Zahle", "Sidon", "Aley", "Tyre", "Nabatieh", "Jounieh", "Batroun", "Baalbeck"), -->
<!-- 		Governorate = c("Beirut", "North", "Beqaa", "South", "Mount Lebanon", "South", "Nabatieh", "Mount Lebanon", "North", "Bekaa"), -->
<!-- 		Population = c(1916100, 730000, 85000, 75000, 65000, 60204, 50000, 35500, 35312, 10392)) -->
		
<!-- cities -->
<!-- ``` -->

## {.tabset}

### task

Construct a data frame of monthly climate statistics in Beirut (source: Wikipedia).
First 5 columns represent temperature in degree celcius. 
Columns 6-7 represent Average precipitation mm (inches)  and Average precipitation days (≥ 0.1 mm), respectively
Column 8 represent Average relative humidity (%) 
Column 9 represent Mean monthly sunshine hours 

### solution

```{r}
climate = data.frame (
    record_high = c(27.9, 30.5, 36.6, 39.3, 41.1, 40.0, 40.4, 39.5, 37.5, 37.0, 33.1, 30.0),
    average_high = c(17.4, 17.5, 19.6, 22.6, 25.4, 27.9, 30.0, 30.7, 29.8, 27.5, 23.2, 19.4),
    daily_mean = c(14.0, 14.0, 16.0, 18.7, 21.7, 24.9, 27.1, 27.8, 26.8, 24.1, 19.5, 15.8),
    average_low = c(11.2, 11.0, 12.6, 15.2, 18.2, 21.6, 24.0, 24.8, 23.7, 21.0, 16.3, 12.9),
    record_low = c(0.4, 3.0, 0.2, 7.6, 10.0, 15.0, 18.0, 19.0, 17.0, 11.1, 7.0, 4.6),
    precipitation = c(190.9, 133.4, 110.8, 46.3, 15.0, 1.5, 0.3, 0.4, 2.3, 60.2, 100.6, 163.8),
    precipitation_days = c(15, 12, 9, 5, 2, 0, 0, 0, 1, 4, 8, 12),
    humidity = c(69, 68, 67, 69, 71, 71, 73, 73, 69, 68, 66, 68),
    sunshine  = 	c(131, 143, 191, 243, 310, 348, 360, 334, 288, 245, 200, 147)
)
rownames(climate) = c("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
```

## {.tabset}

### task

1) Construct a list which contains dates in each month this year.

2) Construct a list which contains some of the following description of yourself:

- name (character)
- month of birth (numeric)
- interest (more than one)
- do you like programming (the value should be logical)

### solution 1 

```{r}
date = list(
	Jan = 1:31, 
	Feb = 1:28, 
	Mar = 1:31, 
	Apr = 1:30, 
	May = 1:31,
	Jun = 1:30,
	Jul = 1:31,
	Aug = 1:31, 
	Sep = 1:30,
	Oct = 1:31,
	Nov = 1:30,
	Dec = 1:31
)
```

### solution 2

```{r}
me = list(
	name = "Pierre Khoueiry",
	month = 9,
	interest = c("jogging", "Badminton"),
	like_programming = TRUE
)

me
me$name
```


## Descriptive statistics

## {.tabset}

### task

With `climate`, calculate the min/max/mean/median/sd/ of each column:

### solution
No solution is given here. Hint: use avaialble R functions

<!-- ```{r} -->
<!-- mean(climate$average_high) -->
<!-- mean(climate$daily_mean) -->
<!-- mean(climate$precipitation) -->
<!-- # or calculate statistics for all columns at a same time -->
<!-- apply(climate, 2, mean) -->
<!-- apply(climate, 2, median) -->
<!-- apply(climate, 2, sd) -->
<!-- ``` -->


## {.tabset}

### task

Does temperature correlates to the precipitation or sunshine?

### solution

```{r}
cor(climate$daily_mean, climate$precipitation)
cor(climate$daily_mean, climate$sunshine)
```

## {.tabset}

### task
Plot the correlations above.
### hint
Use the `plot` function in R.

## {.tabset}

### task

Does the precipitation in May, June, July, Octorber, September
is different to other months?

### solution

```{r}
precipitation = climate$precipitation
t.test(precipitation[5:10],
	   precipitation[c(1:4, 11:12)])
```


## {.tabset}

### task
Which month has the min and max daily means

### hint 
Explore functions similar to `which`



## Plotting climate data

## {.tabset}

### task
Create a boxplot for all temperature columns in set `climat`

### hint
Use boxplot function

## {.tabset}

### task
Plot "record high" and "record low" by month


### solution 1
```{r}
m = factor(rownames(climate), levels = rownames(climate))

plot(as.numeric(m), climate$record_high, type = "b", xaxt = "n", xlab = "Month", ylab = "Record High", col = "red")
## Add custom x-axis with labels
lines(as.numeric(m), climate$record_low, type = "b", xaxt = "n", xlab = "Month", ylab = "Record High", col = "blue")
axis(1, at = as.numeric(m), labels = as.character(m));
```

As you noticed, we are unable to see record_low data! Give an explanation for that.

### solution 2

```{r}
plot(as.numeric(m), climate$record_high, type = "b", xaxt = "n", xlab = "Month", ylab = "Record High", col = "red", ylim = c(min(climate$record_high, climate$record_low), c(max(climate$record_high, climate$record_low))))
## Add custom x-axis with labels
lines(as.numeric(m), climate$record_low, type = "b", xaxt = "n", xlab = "Month", ylab = "Record High", col = "blue")
axis(1, at = as.numeric(m), labels = as.character(m));
```

## {.tabset}

### task 
Plot `precipitation_days` from `climate` as a barplot colored in "lightblue" and add legends for x and y axis as well as a title for the plot

### hint
use `barplot` function in R after reading its help page


### 



