# Whole Exome Sequencing data analysis Hands-on

[comment]: <> (reference: https://galaxyproject.github.io/training-material/topics/variant-analysis/tutorials/exome-seq/tutorial.html)

## __*Introduction*__
"*Exome sequencing is a method that enables the selective sequencing of the exonic regions of a genome - that is the transcribed parts of the genome present in mature mRNA, including protein-coding sequences, but also untranslated regions (UTRs).*"

"*In humans, there are about 180,000 exons with a combined length of ~ 30 million base pairs (30 Mb). Thus, the exome represents only 1% of the human genome, but has been estimated to harbor up to 85% of all disease-causing variants (Choi et al., 2009).*"

"*Exome sequencing, thus, offers an affordable alternative to whole-genome sequencing in the diagnosis of genetic disease, while still covering far more potential disease-causing variant sites than genotyping arrays. This is of special relevance in the case of rare genetic diseases, for which the causative variants may occur at too low a frequency in the human population to be included on genotyping arrays.*"

"*The data in the pipeline are from https://galaxyproject.github.io/training-material/topics/variant-analysis/tutorials/exome-seq/tutorial.html*"

## __*Access Galaxy*__
To access Galaxy, use the following link: https://usegalaxy.org.au

## __*The Pipeline*__
<details>
<summary>1. Import Data</summary>

* Click on __'Shared Data'__ > __'Histories'__ and then look for __'WES-hands-on'__
* At the upper right corner, click on the __'+'__ sign to import the dataset - __'FASTQ raw data and FASTA'__
</details>

---

<details>
<summary>2. Perform Quality Check</summary>

The data to analyse is ready (check History - right hand pane). To perform Quality Checks on raw data (FASTQ format), do the following:

* Look for __FastQC__ in the list of tools (Left hand pane)
* Click on __FastQC__
* Under __'short read data from your current history'__ section, click on __'Multiple datasets'__ (middle gray button)
* Select all __fastq__ files and hit __Execute__
* The results will be shown in the __History__ in HTML format files

We have now for each __fastq__ file a webpage and raw data file showing all the parameters that have been checked. To generate one __Quality Check report for all samples__, do the following:

* Look for __MuliQC__ tool
* Under the section __'Which tool was used generate logs?'__ choose __FastQC__
* Select all __RawData__ files and click on __Execute__
</details>

---

<details>
<summary>3. Map Reads to Reference Genome</summary>

After performing quality checks to make sure that reads are of a good enough quality, it is time to __map reads to the reference genome__.

* From the list of tools, look for __Map with BWA-MEM__ and click on it
* Under __'Will you select a reference genome from your history or use a built-in index?'__ section, select __'Use a genome from history and build index'__ and choose the uploaded FASTA file __'hg38_chr8.fasta'__ under __'Use the following dataset as the reference sequence'__ sub-section
* Under __'Select first set of reads'__ section, click on __'Multiple datasets'__ and select all files with __'R1'__ prefix
* Under  __'Select second set of reads'__ section, click on __'Multiple datasets'__ and select all files with __'R2'__ prefix
* Under __'set read groups information'__ section, select __'Set read groups (Picard style)'__
* click on __'Auto-assign'__ for the following sections: __'Read group identifier (ID)'__, __'Read group sample name (SM)'__ and __'Library name (LB)'__
* Finally , click on __'Execute'__.
  
You will see three 'BWA-MEM 'processes running in the __'History'__.
The final output is three __'BAM'__ files, __one file for each sample__.
</details>

---

<details>
<summary>4. Mark Duplicate Reads</summary>

In this step, we will tag all duplicate reads that are originating from one DNA fragment in BAM files. Duplication can arise during library preparation (PCR)

* From the list of tools click on __Picard__ > __Markduplicates__ 
* Under __'Select SAM/BAM dataset or dataset collection'__ click on __'Multiple datasets'__ and select all three __BAM__ files generated from __BWA-MEM__ alignment step
* Hit __'Execute'__

The output is three __BAM__ files with tagged duplicated reads.
</details>

---

<details>
<summary>5. Call Variants</summary>

In this step, we will use __FreeBayes__ variant caller. it uses the generated BAM files from __MarkDuplication__ step to detect SNPs (Single Nucleotide Polymorphisms), INDELs (Insertions and Deletions) and MNPs (Multi-nucleotide polymorphisms)

* Under the __'Variant Calling'__ section, click on __'Freebayes'__
* Under __'Choose the source for the reference genome'__ section, choose __'History'__
* Under __'run in batch mode'__ section, select __'Run individually'__
* Under __'BAM dataset'__ section, click on __'Multiple datasets'__ and select the three __BAM__ files generated from the last step
* Hit __'Execute'__

The output of this step is three VCF files( __V__ ariant __C__ alling __F__ ormat) , one file for each sample.
</details>

---

<details>
<summary>6. Annotate Variants</summary>

In this step, we annotate the variants (mutations) found. When we say  __'Annotating variants'__, it means adding additional information about the variants. for instance, the affected gene, the consequence of such mutation, the clinical significance and more.

To annotate __Variants__, we will use a tool called __'SnpEff'__

* Under the section called __'Annotation'__, click on __'SnpEff Variant effect and annotation'__ 
* Under the section __'Sequence changes (SNPs, MNPs, InDels)'__, click on __'Multiple datasets'__ and select all three VCF files
* Under __'Genome source'__, choose __'Download on demand'__
* Under the sub-section __'Genome'__, choose __'GRCh38.86'__
* Scroll down and hit __'Execute'__

The output is three new annotated __VCF__ files and their __HTML__ summary reports
</details>

---
