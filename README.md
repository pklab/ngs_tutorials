# NGS_Tutorials

Several tutorials targeting biochemistry students introducing basic analysis in NGS using Galaxy and R (RStudio):

[bioc326 practicals](bioc326/bioc326_practicals.md)

[R basics](R_basic/R_Basic_Manipulation.Rmd)

[Whole Exome Sequencing - hands-on](whole_exome_sequencing/wes_hands_on.md)